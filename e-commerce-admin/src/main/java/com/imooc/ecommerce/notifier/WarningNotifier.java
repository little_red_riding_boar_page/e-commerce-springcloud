package com.imooc.ecommerce.notifier;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.notify.AbstractEventNotifier;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * @program: e-commerce-springcloud
 * @ClassName WarningNotifier
 * @description: 自定义服务告警通知
 * @author: sense
 * @create: 2023-06-07 22:08
 * @Version 1.0
 **/
@Slf4j
@Component
@SuppressWarnings("all")
public class WarningNotifier extends AbstractEventNotifier {
    protected WarningNotifier(InstanceRepository repository) {
        super(repository);
    }

    /**
     * 实现对事件的通知
     * @param event 实例发生的事件
     * @param instance 具体的实例
     * @return
     */
    @Override
    protected Mono<Void> doNotify(InstanceEvent event, Instance instance) {

        return Mono.fromRunnable(() -> {
            if(event instanceof InstanceStatusChangedEvent){
                // 监听到服务状态发生变化后的一些操作
                log.info("Instance Status Change: [{}],[{}],[{}]",instance.getRegistration().getName(),event.getInstance(),((InstanceStatusChangedEvent) event));
            }else {
                log.info("Instance Info: [{}],[{}],[{}]",instance.getRegistration().getName(),event.getInstance(),event.getType());
            }
        });
    }
}
