package com.imooc.ecommerce.service;

import com.immoc.ecommerce.vo.UsernameAndPassword;

/**
 * @program: e-commerce-springcloud
 * @ClassName IJWTService
 * @description: JWT相关服务接口定义
 * @author: sense
 * @create: 2023-06-12 12:41
 * @Version 1.0
 **/
public interface IJWTService {

    /**
     * 生成JWT Token，使用默认的超时时间
     * @param userName
     * @param password
     * @return
     */
    String generateToken(String userName, String password) throws Exception;

    /**
     * 生成JWT Token，超时时间单位为天
     * @param userName
     * @param password
     * @param expire
     * @return
     */
    String generateToken(String userName, String password, int expire) throws Exception;

    /**
     * 注册用户并生成Token返回
     * @param usernameAndPassword
     * @return
     */
    String registerUserAndGenerateToken(UsernameAndPassword usernameAndPassword) throws Exception;
}
