package com.imooc.ecommerce.constant;

/**
 * <h1>授权需要使用的一些常量信息</h1>
 * */
public final class AuthorityConstant {

    /** RSA 私钥, 除了授权中心以外, 不暴露给任何客户端 */
    public static final String PRIVATE_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCnErB6G3x9hsRuFmFApKhc8KUT1CerftlBy5X2ltk9jMfCebh" +
            "GVKD/rYVTyXiprn8kCP9rAcG3DkBx816dRLRUIF8ytX4iG0e5u0Nu0MYepaj4Uv589l4IN6QFYRJtoBwPMrCmdK8ujQBf7C4A5S" +
            "4ASgU797ktF079Uz46zyKDZBaDUxJFyF6Frv0uv9vFSL2P2lfUlcvvFVG+SzhyutPrQDrfCBNc2zrkb0Xtdvp/y1l+KJ4lUQeCE" +
            "WZ0DxGc1XCHpiOlhf3FC5Z469AS7G7xx+3cJ/NGd1dbI4wxPtPhdNEpa5nwB4Y8WfFXYEMTPK1LQourE0u6S16y96tZXqGJAgMB" +
            "AAECggEBAKVp1Zwv1oOZiKXONp9/5smN4ILPTC95ObTw0hsAozlTRvv+8zl+1KAPtq+MC5FEGNEer3PMm3SYAapQYF6pBTk9i8Ny" +
            "+dze0ZbMFymCPoVdUvv5f8lNSzArlpNU1p//I4jQkSFc6YzXQYW3Gpz8Sx0yLBiBXa+NIkGhxz0Gk48tT7d5y4VOrgbbYySKDb7O" +
            "jLfAn7xAbsgvA3VJVD8TnN3/yRCPjOZaxfycATQ1eRSYPqNRFgrhhi49ViXBb7O8MftyNuLZFJa6tVLYyrHUIEQxmhxJpgFGG2/" +
            "aGTliAb6Y2PWAFhEVCgJKEPvSD8VqDmYb0QFlfRucJXz5JyQke+ECgYEA5B5zM7N+H++luCYeqQ2AWqaFg6EOu4sYsrvgKogybK" +
            "O0yg8/0/1oJSKy/HsdNXDh+Gpm+/xqsIaaZaytx8KXSvVayhX8fjNygsj2I3eYo4f3boneM9lYXBgOzVHNx/mhoNe8iwM95x7ZuI" +
            "mI8zxV9JF3Wsd4QcQdDhIZvccAiDUCgYEAu34woqUHoBoZK2ehNnhzSVAIQEcQgFGQC7APJwnqObY71Z9myna0dWc0brCzZ1rtDz" +
            "BGUbKifaXgK2qL/tSV4qCtrOII/92KmuOw6Hav8Umo/LK84Xae/ZL+HFM91sJHQ3v/c3rvcAqmSCr2tfi+053oK1pWAi9WLsG+xF" +
            "ZgJoUCgYEAvnwizIhUiExwAE5sidPJW2jz0k954ucqr40sibeDC67Dt3p2XQmcnm3UMfqIEGj7eJk496/2UwODB+Xikw0Nkr1Am" +
            "8EyPxuswkMXLdSfM+bEmZwsXPVf0D8SlSZ7BEKSUaH0QcxqM4VSXsMSBbJbQEb7+GTaKWiernI05oC3FAUCgYApdm2Q+kfmav8X" +
            "7zHilt5/3vgEJvMKPX9KZmPjcTjgDGHNCxQzP+8Ga41X0THLcHsuObHDTWrLuIwLbeOJtj9zP69IpS/yTW7ic+nibqTYCj8Zqvl" +
            "ELvRYBVK2tt3dCVezH8/NHOmocGJ3YTDv6xNgPvJh6/dLQmafrm7egm9+9QKBgQDBshyiYGJKjzxPdvMpHQTJFxhIgLTgvDIE6qW" +
            "v0AUDozCcGq5e0WvlEq3x8dnM8sOLUe0Hq02oH3O927xdMzNcQdhPq1z8FSdj+2xjwrK8VchRwADuCY0aCA8mpKII3rgIPMJUo" +
            "UbdeimJrRXW/N0oVtcPqiR56exjwPsz29MKsQ==";

    /** 默认的 Token 超时时间, 一天 */
    public static final Integer DEFAULT_EXPIRE_DAY = 1;
}
