package com.imooc.ecommerce;

import cn.hutool.crypto.digest.MD5;
import com.alibaba.fastjson.JSON;
import com.imooc.ecommerce.dao.EcommerceUserDao;
import com.imooc.ecommerce.entity.EcommerceUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @program: e-commerce-springcloud
 * @ClassName AuthorityCenterApplicationTests
 * @description: 验证授权中心环境可用性
 * @author: sense
 * @create: 2023-06-11 22:38
 * @Version 1.0
 **/
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorityCenterApplicationTests {

    @Autowired
    private EcommerceUserDao ecommerceUserDao;

    @Test
    public void contextLoad(){

    }
}
