package com.imooc.ecommerce;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @program: e-commerce-springcloud
 * @ClassName GatewayApplicationTest
 * @description: 验证工程搭建的正确性测试用例
 * @author: sense
 * @create: 2023-06-19 14:46
 * @Version 1.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class GatewayApplicationTest {

    @Test
    public void contextLoad(){

    }
}
