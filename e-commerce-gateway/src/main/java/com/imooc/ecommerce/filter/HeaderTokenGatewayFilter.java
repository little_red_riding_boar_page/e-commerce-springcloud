package com.imooc.ecommerce.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @program: e-commerce-springcloud
 * @ClassName HeaderTokenGatewayFilter
 * @description: HTTP 请求头部携带 Token 验证过滤器
 * @author: sense
 * @create: 2023-06-27 14:19
 * @Version 1.0
 **/

/**
 * 实现一个自定义的Gateway过滤器
 *  1.创建过滤器类（例如：HeaderTokenGatewayFilter），实现GatewayFilter，Ordered，重写filter方法，加入校验逻辑
 *  2.创建对应的过滤器工厂（例如：HeaderTokenGatewayFilterFactory），将过滤器工厂加入到Spring容器中去
 *  3.在配置文件中进行配置，如果不进行配置，过滤器同样不会生效
 */
public class HeaderTokenGatewayFilter implements GatewayFilter, Ordered {
    /**
     * Filter中的验证流程
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 从HTTP Header中寻找 key 为 token ， value 为 imooc 的键值对
        String name = exchange.getRequest().getHeaders().getFirst("token");
        if("imooc".equals(name)){
            return chain.filter(exchange);
        }

        // 标记此次请求没有权限，并结束这次请求
        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        return exchange.getResponse().setComplete();
    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE + 2;
    }
}
