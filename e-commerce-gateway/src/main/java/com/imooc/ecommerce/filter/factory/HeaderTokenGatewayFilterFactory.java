package com.imooc.ecommerce.filter.factory;

import com.imooc.ecommerce.filter.HeaderTokenGatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

/**
 * @program: e-commerce-springcloud
 * @ClassName HeaderTokenGatewayFilterFactory
 * @description:
 * @author: sense
 * @create: 2023-06-27 14:30
 * @Version 1.0
 **/
@Component
public class HeaderTokenGatewayFilterFactory  extends AbstractGatewayFilterFactory<Object> {
    @Override
    public GatewayFilter apply(Object config) {
        return new HeaderTokenGatewayFilter();
    }
}
