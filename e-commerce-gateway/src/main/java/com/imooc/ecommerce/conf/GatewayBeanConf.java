package com.imooc.ecommerce.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @program: e-commerce-springcloud
 * @ClassName GatewayBeanConf
 * @description: 网关需要注入到容器中的 Bean
 * @author: sense
 * @create: 2023-06-27 14:56
 * @Version 1.0
 **/
@Configuration
public class GatewayBeanConf {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
