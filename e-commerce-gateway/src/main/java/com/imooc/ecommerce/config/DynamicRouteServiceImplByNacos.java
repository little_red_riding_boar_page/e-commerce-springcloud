package com.imooc.ecommerce.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * @program: e-commerce-springcloud
 * @ClassName DynamicRouteServiceImplByNacos
 * @description: 通过 nacos 下发动态路由配置，监听Nacos中路由器的配置变更
 * @author: sense
 * @create: 2023-06-20 13:26
 * @Version 1.0
 **/
@Slf4j
@Component
@DependsOn({"gatewayConfig"})
public class DynamicRouteServiceImplByNacos {

    /**
     * Nacos 配置服务
     */
    private ConfigService configService;

    private final DynamicRouteServiceImpl dynamicRouteService;

    public DynamicRouteServiceImplByNacos(DynamicRouteServiceImpl dynamicRouteService) {
        this.dynamicRouteService = dynamicRouteService;
    }

    /**
     * Bean 在容器中构造完成后会执行 init 方法
     */
    @PostConstruct
    public void init(){
        log.info("gateway route init....");
        try{
            // 初始化 Nacos 客户端
            configService = initConfigService();
            if(Objects.isNull(configService)){
                log.error("init config service fail");
                return;
            }
            // 通过Nacos Config 并指定路由配置路径去获取路由配置
            String configInfo = configService.getConfig(GatewayConfig.NACOS_ROUTE_DATA_ID,
                    GatewayConfig.NACOS_ROUTE_GROUP,
                    GatewayConfig.DEFAULT_TIMEOUT);
            log.info("get current gateway config:[{}]",configInfo);
            List<RouteDefinition> routeDefinitions = JSON.parseArray(configInfo, RouteDefinition.class);
            log.info("update route:[{}]",routeDefinitions.toString());
            // 将从Config中获取到的路由配置添加到Gateway中
            if(!CollectionUtils.isEmpty(routeDefinitions)){
                routeDefinitions.forEach(rd ->{
                    log.info("init gateway config : [{}]",rd.toString());
                    dynamicRouteService.addRouteDefinition(rd);
                });
            }
        }catch (Exception e){
            log.error("gateway route init has some error:[{}]",e.getMessage(),e);
        }

        // 设置config中配置变更的监听器，实时监听路由的变化
        dynamicRouteByNacosListener(GatewayConfig.NACOS_ROUTE_DATA_ID,GatewayConfig.NACOS_ROUTE_GROUP);
    }

    /**
     * 初始化 Nacos Config
     * @return
     */
    private ConfigService initConfigService(){
        try{
            Properties properties = new Properties();
            properties.setProperty("serverAddr",GatewayConfig.NACOS_SERVER_ADDR);
            properties.setProperty("namespace",GatewayConfig.NACOS_NAMESPACE);
            return configService = NacosFactory.createConfigService(properties);
        } catch (NacosException e) {
            log.error("init gateway nacos config error:[{}]",e.getMessage(),e);
            return null;
        }
    }

    private void dynamicRouteByNacosListener(String dataId, String group){
        try{
            // 给Nacos Config 客户端增加一个监听器
            configService.addListener(dataId, group, new Listener() {
                /**
                 * 自己提供线程池执行操作
                 */
                @Override
                public Executor getExecutor() {
                    return null;
                }

                /**
                 * 监听器收到配置更新
                 * @param configInfo Nacos 中最新的配置定义
                 */
                @Override
                public void receiveConfigInfo(String configInfo) {
                    log.info("start to update config:[{}]",configInfo);
                    List<RouteDefinition> routeDefinitions = JSON.parseArray(configInfo, RouteDefinition.class);
                    log.info("update route:[{}]",routeDefinitions.toString());
                    // 更新Gateway中的路由信息
                    dynamicRouteService.updateList(routeDefinitions);
                }
            });
        }catch (Exception e){

        }
    }


}
