package com.imooc.ecommerce.advice;

import com.immoc.ecommerce.vo.CommonResponse;
import com.imooc.ecommerce.annotation.IgnoreResponseAdvice;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.net.URI;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: e-commerce-springcloud
 * @ClassName CommonResponseDataAdvice
 * @description: 实现统一响应
 * @author: sense
 * @create: 2023-06-05 17:05
 * @Version 1.0
 **/
@RestControllerAdvice("com.imooc.ecommerce")
public class CommonResponseDataAdvice  implements ResponseBodyAdvice<Object> {
    /**
     * 判定是否对响应进行处理
     * @param methodParameter
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        // 获取当前Controller对应的类
        Class<?> declaringClass = methodParameter.getDeclaringClass();
        // 判定当前类是否有忽略响应处理的注解
        if(declaringClass.isAnnotationPresent(IgnoreResponseAdvice.class)){
            return false;
        }
        if(methodParameter.getMethod().isAnnotationPresent(IgnoreResponseAdvice.class)){
            return false;
        }
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // 定义最终返回对象
        CommonResponse<Object> commonResponse = new CommonResponse<>();

        if(Objects.isNull(o)){
            return  commonResponse;
        }else if(o instanceof CommonResponse){
            commonResponse = (CommonResponse<Object>) o;
        }else {
            commonResponse.setData(o);
        }
        return commonResponse;
    }
}
