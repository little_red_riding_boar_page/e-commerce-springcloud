package com.immoc.ecommerce.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: e-commerce-springcloud
 * @ClassName CommonResponse
 * @description: 通用响应对象定义
 * @author: sense
 * @create: 2023-06-05 16:29
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResponse<T> implements Serializable {
    /**
     * 状态码
     */
    private Integer code;

    /**
     * 响应信息
     */
    private String message;

    /**
     * 泛型响应数据
     */
    private T Data;

    public CommonResponse(Integer code,String message){
        this.code = code;
        this.message = message;
    }
}
