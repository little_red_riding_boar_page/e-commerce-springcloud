package com.immoc.ecommerce.constant;

/**
 * <h1>通用模块常量定义</h1>
 * */
public final class CommonConstant {

    /** RSA 公钥 */
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApxKweht8fYbEbhZhQKSoXPClE9Qnq37ZQcu" +
            "V9pbZPYzHwnm4RlSg/62FU8l4qa5/JAj/awHBtw5AcfNenUS0VCBfMrV+IhtHubtDbtDGHqWo+FL+fPZeCDekBWESbaAcDz" +
            "KwpnSvLo0AX+wuAOUuAEoFO/e5LRdO/VM+Os8ig2QWg1MSRcheha79Lr/bxUi9j9pX1JXL7xVRvks4crrT60A63wgTXNs65" +
            "G9F7Xb6f8tZfiieJVEHghFmdA8RnNVwh6YjpYX9xQuWeOvQEuxu8cft3CfzRndXWyOMMT7T4XTRKWuZ8AeGPFnxV2BDEzyt" +
            "S0KLqxNLuktesverWV6hiQIDAQAB";

    /** JWT 中存储用户信息的 key */
    public static final String JWT_USER_INFO_KEY = "e-commerce-user";

    /** 授权中心的 service-id */
    public static final String AUTHORITY_CENTER_SERVICE_ID = "e-commerce-authority-center";
}
